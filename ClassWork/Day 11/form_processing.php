<?php
    class Student{
        public $math,$english,$bengali,$physics;

        public function __construct($math_marks, $english_marks, $bengali_marks, $physics_marks)
        {
            $this->math = $math_marks;
            $this->english = $english_marks;
            $this->bengali = $bengali_marks;
            $this->physics = $physics_marks;
        }
        private function calculateGrades(){
            if($this->math < 70 || $this->english_marks < 70 || $this->bengali_marks < 70 || $this->physics_marks < 70 ){
                echo "Grade: Fail";
            }
            else if($this->math_marks >= 90 && $this->english_marks >= 90 && $this->bengali_marks >= 90 && $this->physics_marks >= 90){
                echo "Grade: Golden A+";
            }
            else if($this->math_marks >= 80 && $this->english_marks >= 80 && $this->bengali_marks >= 80 && $this->physics_marks >= 80){
                echo "Grade: A+";
            }
            else{
                echo "Grade: A";
            }
        }
        public function getTotalMarks(){
            echo "Total Mark is: ";
            echo $this->math_marks + $this->english_marks + $this->bengali_marks + $this->physics_marks;
            echo "<br>";
        }
        public function getGrade(){
            $this->calculateGrades();
        }
    }

    $newStudent = new Student($_GET['math'],$_GET['english'],$_GET['bengali'],$_GET['physics']);
    $newStudent->getTotalMarks();
    $newStudent->getGrade();
?>