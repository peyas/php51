<?php
class carTank{
    public $tank;
    private $consumedFuel;
    public function fill($fillAmount){
        $this->tank = $fillAmount;
        return $this;
    }
    public function ride($distanceRide){
        $this->consumedFuel = $distanceRide/50;
        $this->tank -= $this->consumedFuel;
        return $this;
    }
}

$bus = new carTank();
echo $bus->fill(20)->ride(50)->tank;
?>