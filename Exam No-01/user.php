<?php


class User
{
    private $userName;
    public function setUsername($x) {
        $this->userName = $x;
    }
    protected function getUserName(){
        return $this->userName;
    }
}

class Admin extends User{
    public function ofexpressYourRole(){
        return "Admin";
    }
    public function sayHello(){
        return "Helloadmin, ".$this->getUserName();
    }
}
$admin1 = new Admin();
$admin1->setUserName("Balthazar");
echo $admin1->sayHello();
?>